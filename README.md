/* Adding README file */

This is a simple HTML5 project which will list down the itunes listing of music.

Landing Page simply contains a button which when clicked will open another page in overlay (without page refresh). This page in overlay will as Artist Name and number of listing you want to list. 
Both the fields are required, if not provided it will show the error in same page. Once both the fields are provided, it will make an ajax call to itunes server while front-end shows a elegant loader.

Once the results are fetched, overlay page will automatically close and results will be displayed on the page already loaded. If there are no results fetched, relevant message will be shown.